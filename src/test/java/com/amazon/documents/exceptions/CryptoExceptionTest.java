package com.amazon.documents.exceptions;

import com.amazon.documents.exception.CryptoException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertSame;

class CryptoExceptionTest {

    @Test
    public void testConstructor() {
        Throwable throwable = new RuntimeException();
        CryptoException exception = new CryptoException(throwable);
        assertSame(throwable, exception.getCause());
    }
}
