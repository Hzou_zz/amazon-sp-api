
# Item

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asin** | **String** |  | 
**attributes** | [**ItemAttributes**](ItemAttributes.md) |  |  [optional]
**identifiers** | [**ItemIdentifiers**](ItemIdentifiers.md) |  |  [optional]
**images** | [**ItemImages**](ItemImages.md) |  |  [optional]
**productTypes** | [**ItemProductTypes**](ItemProductTypes.md) |  |  [optional]
**salesRanks** | [**ItemSalesRanks**](ItemSalesRanks.md) |  |  [optional]
**summaries** | [**ItemSummaries**](ItemSummaries.md) |  |  [optional]
**variations** | [**ItemVariations**](ItemVariations.md) |  |  [optional]
**vendorDetails** | [**ItemVendorDetails**](ItemVendorDetails.md) |  |  [optional]



